from django.apps import AppConfig


class WedsiteConfig(AppConfig):
    name = 'wedsite'
