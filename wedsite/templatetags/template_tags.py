from django import template


register = template.Library()


@register.inclusion_tag('_nav.html', takes_context=True)
def nav_loader(context, request):
    return {
        'path': request.path,
    }
